package com.mercury.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public  class FileReading1 {
	public String Excel_File_Reading(String FilePath,int sheetnum,int row,int column) throws IOException{
     File file= new File(FilePath);
     FileInputStream fs = new FileInputStream(file);
     XSSFWorkbook wb= new XSSFWorkbook(fs);
     XSSFSheet sheet = wb.getSheetAt(sheetnum);
     String data = sheet.getRow(row).getCell(column).getStringCellValue();
      return data;

	        }
	}
